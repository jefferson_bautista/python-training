"""
Exercise 1.1:
------------------------
Modify the code below to ask the user to input their birth year 
and the integer representation of their birth month (January=1, February=2, etc.)
then compute the age of the user and print it out.

Assume that the current year is 2020 and current month is 1.

Sample run:
```
	>>> Input your birth year: 1998
	Your birth year is 1998
	>>> Input your birth month: 4
	Your birth month is 4
	Calculating...
	Your age is 21

	>>> Input your birth year: 2005
	Your birth year is 2005
	>>> Input your birth month: 1
	Your birth month is 1
	Calculating...
	Your age is 15
```

Goals:
-------------------------
To be familiarize on multiple python functionalities such as:
1. User input
2. Mathematical operators
3. Variables

Notes:
-------------------------
> You can answer this without using conditional statments. But I am not preventing you to use them.
> You have the freedom to change the output messages from the sample run.
> Always follow the PEP 8 coding standards

"""


def calculate_age():
	current_year = 2020
	current_month = 1
	
if __name__ == "__main__":
	calculate_age()
	