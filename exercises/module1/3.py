

"""
Excercise 1.3
-------------------------
Pattern printing:

Create a python scripts that accepts three inputs and creates a pattern using these inputs:
- symbol
- number of rows
- number of columns
The pattern is rows x columns grid of symbols. See example below

Sample run:
```
    >>> Enter symbol: @
    >>> Enter rows: 3
    >>> Enter columns: 3

    @@@
    @@@
    @@@

    
    >>> Enter symbol: a
    >>> Enter rows: 2
    >>> Enter columns: 4

    aaaa
    aaaa
```

Goals:
---------------------------
To familiarize on the following Python functionalities:
1. Data type casting
2. String concatenation and repetition

Notes:
---------------------------
> AVOID USING LOOPS.
> Always follow the Pep 8 coding standards

"""

def generate_pattern():
    symbolinput("Enter symbol: ")
    row = int(input("Enter rows: "))
    col = int(input("Enter columns: "))
    

if __name__ == "__main__":
    generate_pattern()
