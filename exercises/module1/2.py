"""
Exercise 1.2:
------------------------
There are 3 students in the database of Jeff University. They are the following:
    Student name            Id Number
    Marielle                0112345
    Michael                 0112346
    Venedick                0112347

At the start of the new academic year there will be new students that need to be inserted to the database with their assigned id numbers.
Create a python script that will accept a new student as an input from the user, 
the script will then increment the last Id number by 1 from the database of Jeff university and assign it to the new student.

Sample run:
```
    >>> New student: Ruby
    Assigning student...
    Ruby : 0112348

    >>> New student: Archelle
    Assigning student...
    Archelle: 0112348
```

Goals:
-------------------------
To familirize on the following Python functionalities:
1. Data type casting
2. String concatenation
3. Mathematical operators

Notes:
-------------------------
> The last id is already given in the code below, it is in string do not change it.
    This is only a simulation of inserting one student no need to add a loop, or list. 
    Don't overthink this simple exercise.

> Always follow the PEP 8 coding standards.
> You have the freedom to change the output messages from the sample run.

"""



def generate_new_id():
    last_id = "0112347"
    


if __name__ == "__main__":
    generate_new_id()

