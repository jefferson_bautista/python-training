"""
Exercise 3.3
----------------------
2d Hiker part 2

Modify your code from Exercise 2.1 so that it can have the following feature:
    - Have an infinite loop to allow user to play the 2d Hiker game, the loop only ends when the user finished the game.
    - The user will input a number which corresponds the number of steps that the hiker will take. 
    - A single step is equivalent to a single checkpoint.
    - The hiker can skip checkpoints.
    - Once the user reached the summit, compute all the distance values of all the checkpoints the hiker have reached.
    - The hiker can "fall off" when he overshoots the summit, when the steps he took is greater than the required steps to reach the summit.

Sample run:
```
    >>> Enter checkpoint distances: 1-2-10-55
    hiker - 1 - 2 - 10 - 55 - summit

    >>> How many steps to take: 2
    x - 1 - hiker - 10 - 55 - summit

    >>> How many steps to take: 3
    x - 1 - x - 10 - 55 - hiker
    Hurray! You have reached the summit:
    Total distance covered: 2


    >>> Enter checkpoint distances: 1-2-5-10-7
    hiker - 1 - 2 - 5 - 10 - 7 - summit

    >>> How many steps to take: 1
    x - hiker - 2 - 5 - 10 - 7 - summit

    >>> How many steps to take: 3
    x - x - 2 - 5 - hiker - 7 - summit

    >>> How many steps to take: 2
    x - x - 2 - 5 - x - 7 - hiker
    Hurray! You have reached the summit:
    Total distance covered: 11


    >>> Enter checkpoint distances: 1-2-5
    hiker - 1 - 2 - 5  summit

    >>> How many steps to take: 1
    x - hiker - 2 - 5 - summit

    >>> How many steps to take: 4
    Uh oh! You have fallen off the mountain! Game over 

```
"""