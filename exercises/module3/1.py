"""
Exercise 3.1
--------------------------------
This is a continuation of the Jeff University system.

Modify your code from Exercise 2.1 to allow an infinite loop so that the system will always listen for a user input.
By doing so, the system can now also retain data in the datalist as long as the system is running.

Two new columns requested to be added in the datalist of students:
1. birthdate - User will input their birthdate in the format of YYYY-MM-DD
2. age - The system will be the one responsible to compute the age of the student.

Sample run:
```
    >>> New student: Ruby
    >>> Enter birthdate: 2020-01-02
    Assigning student...
    Ruby: 0112348

    Updating datalist...
    Here is the updated datalist:
    ----------------------------
    Student name            
    Marielle               
    Michael                 
    Venedick
    Ruby                
    ----------------------------
    Id Number
    0112345
    0112346
    0112347
    0112348
    ----------------------------
    Birthdate
    1990-04-04
    1945-11-23
    1990-03-21
    2010-01-02
    ----------------------------
    Computed age
    30
    74
    30
    10

    >>> New student: Archelle
    >>> Enter birthdate: 1994-12-25
    Assigning student...
    Archelle: 0112349

    Updating datalist...
    Here is the updated datalist:
    ----------------------------
    Student name            
    Marielle               
    Michael                 
    Venedick
    Ruby  
    Archelle              
    ----------------------------
    Id Number
    0112345
    0112346
    0112347
    0112348
    0112349
    ----------------------------
    Birthdate
    1990-04-04
    1945-11-23
    1990-03-21
    2020-01-02
    1994-12-25
    ----------------------------
    Computed age
    30
    74
    30
    10
    25
```


"""