"""
Exercise 3.4
----------------------
Jepipi number

Create a python script that will detect whether a number is a jepipi number or a non-jepipi number.

You can validate a jepipi number by doing the following steps:
1. Start with a positive integer.
    Ex: 49
2. Replace the number by the sum of the squares of every digit.
    Ex: 
        49 = (4)**2 + (9)**2 
           = 97

3.1. Repeat the process until the output is 1.
3.2. If one of the output is a number that was already touched at least once, then it is a non-Jepipi number.

Sample computaion:
19
= (1)**2 + (9)**2
= 82
= (8)**2 + (2)**2
= 68
= (6)**2 + (8)**2
= 100
= (1)**2 + (0)**2 + (0)**2
= 1
19 is a Jepipi number

4
= (4)**2
= 16
= (1)**2 + (6)**2
= 37
= (3)**2 + (7)**2
= 58
= (5)**2 + (8)**2
= 89
= (8)**2 + (9)**2
= 145
= (1)**2 + (4)**2 + (5)**2
= 42
= (4)**2 + (2)**2
= 20
= (2)**2 + (0)**2
= 4
4 is a non-Jepipi number since it was already part of the sequence above.

"""

