"""
Exercise 3.5
----------------------
Moving Standard deviation

Create a python script that will compute the moving standard deviation between numbers in a list.
The formula for standard deviation is:

stdDev = square_root( ((sum(X-mean)**2))/N )

Where X is a collection of numbers
sum() is the summation function
mean is the average value of X
N is the number of data


Example:
[1,5,8,22]
Step 1: Compute the standard deviation between 1 and 5
    X = {1, 5}
    mean = 3

    stdDev = square_root( (( (1-3)**2 + (5-3)**2 ))/2 )
            = square_root( 8/2 )
            = 2

Step 2: Compute the standard deviation between the previous standard deviation (2) and the
next number which is 8
    X = {2, 8}
    mean = 5

    stdDev = square_root( ( (2-5)**2 + (8-5)**2 )/2 )
            = square_root( 18/2 )
            = 3
Repeat step 2 until you get all the standard deviations.

Output:
[2, 3, 9.5]
"""
