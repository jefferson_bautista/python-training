"""
Exercise 3.2
--------------------------------
Fizzbuzznacci Sequence

This exercise is a combination of two very common programming problems Fibonacci Sequence and Fizz buzz.

The fibonacci sequence is formed by adding the last two numbers to get the next one, starting from 0 and 1:
0, 1 <~ First two numbers
    Compute the last two numbers: 0 + 1 = 1
0, 1, 1 <~ First three numbers
    Compute the last two numbers: 1 + 1 = 2
0, 1, 1, 2 and it continues...

The fizzbuzz problem goes like this:
 For number which is a multiple of 3, print "Fizz"
 For the number which is a multiple of 5, print "Buzz"
 For numbers which are multiples of both 5 and 3, print "FizzBuzz"

Combine the two problems.
What you are tasked to do is to create a python script that will compute the fibonacci sequence up until to the 30th number (514,229)
at the same time implementing fizzbuzz on the numbers:

Sample output:
```
0, 1, 2, Fizz, Buzz, 8, 13, Fizz, 34, ..., 514229
```


"""