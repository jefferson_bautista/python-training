
chest1 = Chest(double_chest=True)
print(chest1.max_capacity)
"""
54
"""

chest1.put("wood")
chest1.put("pickaxe")
chest1.put("wool")
print(chest1.current_capacity)
"""
3
"""

shulker_box = ShulkerBox()
shulker_box.put("wood")
shulker_box.put("stone")
shulker_box.put("axe")
shulker_box.put("pickaxe")
shulker_box.put("shovel")
shulker_box.put("sword")
shulker_box.put("shield")
shulker_box.put("elytra")
shulker_box.put("rockets")
shulker_box.put("ender pearl")
shulker_box.put("chest")
shulker_box.put("barrel")
shulker_box.put("ender chest")
shulker_box.put("villager spawner")
shulker_box.put("zombie spawner")
shulker_box.put("ravager spawner")
shulker_box.put("piglin spawner")
shulker_box.put("hoglin spawner")
shulker_box.put("pig spawner")
shulker_box.put("chicken spawner")
shulker_box.put("leather")
shulker_box.put("pork")
shulker_box.put("beef")
shulker_box.put("chicken")
shulker_box.put("fish")
shulker_box.put("beacon")
shulker_box.put("nether star")
print(shulker_box.current_capacity)
"""
27
"""

shulker_box.put("dirt")
"""
Storage is full
""
