barrel = Barrel()
barrel.put("wood")
barrel.put("diamond")
barrel.peek()
"""
[wood, diamond]
"""

barrel.pickup()
barrel.peek()
"""
[]
"""

shulker_box = ShulkerBox()
shulker_box.put("leather")
shulker_box.put("soul sand")
shulker_box.peek()
"""
[leather, soul sand]
"""
shulker_box.pickup()
shulker_box.peek()
"""
[leather, soul sand]
"""
