chest1 = Chest()
chest1.put("wood")
chest1.put("stone")
chest1.peek()
"""
[wood, stone]
"""

chest2 = Chest()
chest2.put("bricks")
chest2.put("redstone dust")
chest2.peek()
"""
[bricks, redstone dust]
"""

barrel1 = Barrel()
barrel1.put("concrete")
barrel1.put("stone")
barrel1.peek()
"""
[concrete, stone]
"""

barrel2 = Barrel()
barrel2.put("bricks")
barrel2.put("leaves")
barrel2.peek()
"""
[bricks, leaves]
"""

shulker_box1 = ShulkerBox()
shulker_box1.put("diamond")
shulker_box1.put("stone")
shulker_box1.peek()
"""
[diamond, stone]
"""

shulker_box2 = Chest()
shulker_box2.put("villager spawner")
shulker_box2.put("redstone dust")
shulker_box2.peek()
"""
[villager spawner, redstone dust]
"""

ender_chest1 = EnderChest()
ender_chest1.put("iron")
ender_chest1.put("gold")
ender_chest1.peek()
"""
[iron, gold]
"""

ender_chest2 = EnderChest()
ender_chest2.put("leather")
ender_chest2.put("pork")
ender_chest2.peek()
"""
[iron, gold, leather, pork]
"""

