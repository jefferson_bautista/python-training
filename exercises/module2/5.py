"""
Exercise 3.5
------------------------
Re-do the jepipi latin conversion, but this time the input is in list.
Your output should be in string.
"""

if __name__ == "__main__":
    str1 = ["Hello", "my", "name", "is", "jeff"]
    str2 = ["The", "greatest", "glory", "in", "living", "lies", "not", "in", "never", "falling,", "but", "in", "rising", "every", "time", "we", "fall"]
    str3 = ["If", "you", "set", "your", "goals", "ridiculously", "high", "and", "it", "failed,", "you", "will", "fail", "above", "everyone", "else's", "success"]
    
