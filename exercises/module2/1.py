""" 
Exercise 2.1
------------------------
This is a continuation of Exercise 1.2
Jeff university now implements list to store their students names and id they call it datalist. You are assigned to handle, manage, and 
kickstart the implementation of the datalist. 
You are also given the task to create a system that can add students to the list of students.

Modify your code from Exercise 1.2 to allow the function `generate_new_id` to return the generated new id, for you to be able to store it in a variable. 
Also remove the `input()` inside the function and put it inside the main function.

```
def generate_new_id():
    

    # Your code here

    return generated_id
```

After a few scrum meetings with the Jeff university dev team, here is now the breakdown of your tasks:

1. Initialize a list `student_ids` which contains all the Student ID from Exercise 1.2 
2. Initialize another list `student_names` which contains all the student names from Exercise 1.2
3. Create a function `add_to_datalist` which will add a given `student_data` to a given `student_list` and will return the new `student_list`
    this function will accept two parameters:
        -> student_list - the list where the data will be added
        -> student_data - the data to be added to the list 

    this function will return:
        -> list where the data is already added.

4. Create another function `stringify_list` that will accept a list as a parameter and 
will print out all of values from the list parameter separated by break lines (\n).


Sample run:
```
    >>> New student: Ruby
    Assigning student...
    Ruby : 0112348

    Updating datalist...
    Here is the updated datalist:
    ----------------------------
    Student name            
    Marielle               
    Michael                 
    Venedick
    Ruby                
    ----------------------------
    Id Number
    0112345
    0112346
    0112347
    0112348


    >>> New student: Archelle
    Assigning student...
    Archelle : 0112348

    Updating datalist...
    Here is the updated datalist:
    ----------------------------
    Student name            
    Marielle               
    Michael                 
    Venedick
    Archelle                
    ----------------------------
    Id Number
    0112345
    0112346
    0112347
    0112348
```

Goals:
-------------------------
To familirize on the following Python functionalities:
1. Functions
2. Lists


Notes:
-------------------------
> Read carefully the instructions.
> Call all the functions from the main function.
> Always follow the PEP 8 coding standards.
> You have the freedom to change the output messages from the sample run.
"""

if __name__ == "__main__":
    student_ids = []
    student_names = []

    # TODO Your exercise 1.2 code here

    print("Updating datalist ...")
    student_ids = add_to_datalist()
    student_names = add_to_datalist()

    print("Here is the updated datalist ...")
    stringify_list(student_names)
    stringify_list(student_ids)

