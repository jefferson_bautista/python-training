"""
Exercise 2.3
----------------------------
2D hiker

Create the environment for the game 2D hiker.
The rules are simple.
- A hiker will start at jump-off (index 0)
- The summit is the last stop (index N-1)
- There will be N checkpoints with distance values each.
- N > 1
Visualization:

hiker - 2 - 10 - 7 - 13 - summit

Your python program shoud be able to do the following:
1. Accept an input which is the distance value of the checkpoints in string format, separated by hyphen (-)
2. Save the position of the hiker, the summit and all the checkpoint values in a list (environment).
3. Generate and print the environment based on the input

Sample run:
```
    >>> Enter checkpoint distances: 1-2-10-55
    hiker - 1 - 2 - 10 - 55 - summit

    >>> Enter checkpoint distances: 55-69-100-22
    hiker - 55 - 69 - 100 - 22 - summit
```

Goals
----------------------------
To familiarize on the following Python functionalities:
1. Lists
2. Strings

Notes:
----------------------------
> Read carefully the instructions.
> Call all the functions from the main function.
> Always follow the PEP 8 coding standards.
"""

