"""
Excercise 2.2
------------------------
Use the variable `corpus` for this exercise.
1. Create a function remove_breaks() that will accept the `corpus` variable, the function will remove all the html break symbols (</>) from the corpus then return it.
2. Create a function remove_typo() that will accept the `corpus` variable, the function will then remove one of the two "amet" in the very first line of the `corpus` variable
    ("Lorem ipsum dolor sit amet amet"). This function must return the corpus but with only one amet in the first line.

Goals:
------------------------
1. To make use of python's string functions.

Notes:
------------------------
> Do not change anything from the corpus given below.
> Call all the functions from the main function.
> Always follow the PEP 8 coding standards.
"""


if __name__ == "__main__":
    corpus = """
    Lorem ipsum dolor sit amet amet, </> consectetur adipiscing elit. Quisque risus odio, ullamcorper quis mauris a, ultrices vulputate velit. 
    Nullam condimentum pretium ipsum, in interdum diam consectetur et. Aenean cursus turpis vel malesuada egestas. 
    Curabitur in dui semper, ultricies erat ut, porta odio. Aenean et vulputate arcu. Curabitur ac ex neque. Quisque sagittis gravida dapibus. 
    Sed rutrum ultrices massa. Sed rutrum egestas orci, id auctor mi congue et. Phasellus volutpat leo sed vestibulum luctus. Lorem ipsum dolor sit amet, 
    consectetur adipiscing elit. Maecenas blandit rutrum facilisis. </>

    Maecenas tincidunt enim eget nunc imperdiet, non bibendum tellus iaculis. Suspendisse tellus velit, facilisis in lorem sed, maximus egestas quam. 
    Pellentesque maximus nisi vitae massa eleifend consectetur. </> Praesent vel cursus nisl, non tincidunt elit. Pellentesque vitae dapibus sapien. 
    Nam eget ex sagittis, sodales mi at, elementum odio. Curabitur tincidunt commodo dui. </> Morbi et nibh vestibulum, feugiat sapien non, consectetur erat.
    """
