"""
Exercise 3.4
------------------------
Jepipi Latin:

You are tasked to translate the following engilsh text into jepipi latin:
1. Hello my name is Jeff
2. The greatest glory in living lies not in never falling, but in rising every time we fall
3. If you set your goals ridiculously high and it failed, you will fail above everyone else's success

The steps needed to translate english to jepipi latin are as follows:
1. Find the middle word in the sentence (N/2).
2. Move all the words from the left of N/2 to the right, like wise move all the words from the right of N/2 to the left.
3. Reverse the last word in the sentence.
4. Add "jepipi" at the beginning of the sentence.

Example:
English - Betty Botter bought some butter
Jepipi latin - jepipi some butter bought Betty rettoB


Goals:
-------------------------
To familirize on the following Python functionalities:
1. Functions
2. Strings


Notes:
-------------------------
> Read carefully the instructions.
> Call all the functions from the main function.
> Always follow the PEP 8 coding standards.
> AVOID using 3rd party libraries.
"""

if __name__ == "__main__":
    str1 = "Hello my name is Jeff"
    str2 = "The greatest glory in living lies not in never falling, but in rising every time we fall"
    str3 = "If you set your goals ridiculously high and it failed, you will fail above everyone else's success"
